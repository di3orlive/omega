jQuery.exists = function (selector) { // функция проверки существования селектора
    return ($(selector).length > 0);
};

$(function() {
    $(document).ready(function() {
        $(".player").eq(0).mb_YTPlayer();
    });
//===========================================================================================================
    if($.exists("#main")){
        var oTurn = $("#main").turn({
            display: 'single',
            autoCenter: true,
            next: true,
            duration: 3000
        });

        $(oTurn).turn("peel", "tr");
        $(oTurn).turn("options", {turnCorners: "bl,tr"});

        $(oTurn).bind("start", function (event, pageObject, corner) {
            if (corner == "tl" || corner == "bl" || corner == "br") {
                event.preventDefault();
            }
        });

        $(oTurn).bind("turning", function(event, page, view) {
            $(".close-animatedModal-2").remove();
        });

        $(oTurn).bind("turned", function(event, page, view) {
            $(".player").eq(1).mb_YTPlayer();
            $(".index").remove();
        });

        $(".page.p-temporal").prev().css("cursor", "pointer");

        $("#next").click(function (e) {
            e.preventDefault();
            oTurn.turn("next");
        });
    }



    if($.exists("a.open-box")){
        $('a.open-box').click(function (e) {
            var href = $(e.target).attr('href');
            var block = $(href).parent();
            var insideBlock = $(block).find('.block-content');

            if ($(href).hasClass('active') && $(block).hasClass('active')) {
                $(href).removeClass('active');
                $(block).removeClass('active');
                $(insideBlock).show();
                $('.block:not(block)').removeClass('width0');
            } else {
                $(href).addClass('active');
                $(block).addClass('active');
                $(insideBlock).hide();
                $('.block:not(block)').addClass('width0');
            }
        })
    }


});

